﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonCompany.Models
{
    public class City
    {
        public virtual int IdCity { get; set; }
        public virtual string NameCity { get; set; }
        public virtual int IdCountry { get; set; }
    }

}