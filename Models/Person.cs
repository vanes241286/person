﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonCompany.Models
{
    public class Person
    {
        public virtual int IdPerson { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string NumberPhone { get; set; }
        public virtual string Email { get; set; }
        public virtual int IdCountry { get; set; }
        public virtual int IdCity { get; set; }
        public virtual Country Country { get; set; }
        public virtual City City { get; set; }

    }
}