﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonCompany.Models
{
    public class Country
    {
        public virtual int IdCountry { get; set; }
        public virtual string NameCountry { get; set; }
    }
}