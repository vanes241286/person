﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="PersonCompany._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <script>

        Ext.apply(Ext.form.VTypes, {
            numberphone: function (val, field) {
                if (!val) {
                    return;
                }

                if (! /^\d+$/.test(val)) {
                    this.numberphoneText = 'Должен содержать только цифры';
                    return;
                }

                if (field.value.length != 11) {
                    this.numberphoneText = 'Должен содержать 11 цифры';
                    return;
                }

                return true;
            },
            numberphoneText: 'Должен содержать только цифры'
        });

        var ClickSave = function () {
            if (App.PersonForm.isValid()) {
                App.PersonForm.submit({
                    success: function (f, a) {
                        Ext.Msg.alert('Информация', Ext.JSON.decode(a.response.responseText).d.message);
                        App.PersonStore.load();
                        App.PersonForm.reset(true);
                    },
                    failure: function (f, a) {
                        Ext.Msg.alert('Ошибка', Ext.JSON.decode(a.response.responseText).d.message);
                    }
                });
            }
        }
        var ClickSearch = function () {

            isReload = false;
            App.PersonStore.getProxy().extraParams = new Object();
            if (App.s_LastName.value != '') {
                App.PersonStore.getProxy().extraParams.LastName = App.s_LastName.value;
                isReload = true;
            }
            if (App.s_FirstName.value != '') {
                App.PersonStore.getProxy().extraParams.FirstName = App.s_FirstName.value;
                isReload = true;
            }
            if (App.s_MiddleName.value != '') {
                App.PersonStore.getProxy().extraParams.MiddleName = App.s_MiddleName.value;
                isReload = true;
            }
            if (App.s_NumberPhone.value != '') {
                App.PersonStore.getProxy().extraParams.NumberPhone = App.s_NumberPhone.value;
                isReload = true;
            }
            if (App.s_Email.value != '') {
                App.PersonStore.getProxy().extraParams.Email = App.s_Email.value;
                isReload = true;
            }
            if (App.s_IdCountry.value != null) {
                App.PersonStore.getProxy().extraParams.IdCountry = App.s_IdCountry.value;
                isReload = true;
            }
            if (App.s_IdCity.value != null) {
                App.PersonStore.getProxy().extraParams.IdCity = App.s_IdCity.value;
                isReload = true;
            }
            if (isReload)
                App.PersonStore.reload();
        }

        var ClickPersonClear = function () {
            App.PersonForm.reset(true);
        }

        var ClickSearchClear = function () {
            App.SearchForm.reset(true);
            App.PersonStore.getProxy().extraParams = new Object();
            App.PersonStore.reload();
        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <ext:ResourceManager runat="server" />
        <ext:Model runat="server" Name="MessageModel">
            <Fields>
                <ext:ModelField Name="success" />
                <ext:ModelField Name="message" />
            </Fields>
        </ext:Model>

        <ext:Panel runat="server" Layout="ColumnLayout" Margin="30">
            <Items>
                <ext:Panel runat="server" ColumnWidth="0.15">
                    <Items>
                        <ext:FormPanel
                            ID="PersonForm"
                            runat="server"
                            Title="Cотрудник"
                            ColumnWidth="0.35"
                            BodyPadding="20"
                            JsonSubmit="true"
                            Url="/Services/PersonService.asmx/PersonSave">
                            <Defaults>
                            </Defaults>
                            <Items>
                                <ext:Hidden ID="IdPerson" runat="server" />
                                <ext:TextField ID="LastName" runat="server" Width="220" EmptyText="Фамилия" AllowBlank="false" />
                                <ext:TextField ID="FirstName" runat="server" Width="220" EmptyText="Имя" AllowBlank="false" />
                                <ext:TextField ID="MiddleName" runat="server" Width="220" EmptyText="Отчество" AllowBlank="false" />
                                <ext:TextField ID="NumberPhone" runat="server" Width="220" EmptyText="Номер мобильного телефона" AllowBlank="False" Vtype="numberphone">
                                    <%--                                    <Plugins>
                                        <ext:InputMask ID="InputMask1" runat="server" Mask="9 (999) 999-99-99" Placeholder="_" AlwaysShow="true" />
                                    </Plugins>--%>
                                </ext:TextField>
                                <ext:TextField ID="Email" runat="server" Width="220" EmptyText="Адрес электронной почты" Vtype="email" AllowBlank="false" />
                                <ext:SelectBox ID="IdCountry" runat="server" EmptyText="Страна" Width="220" AllowBlank="false"
                                    DisplayField="NameCountry"
                                    ValueField="IdCountry">
                                    <Store>
                                        <ext:Store runat="server" ID="CountryStore">
                                            <Model>
                                                <ext:Model runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="IdCountry" />
                                                        <ext:ModelField Name="NameCountry" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                            <Proxy>
                                                <ext:AjaxProxy Url="/Services/PersonService.asmx/CountryList" Json="true">
                                                    <Reader>
                                                        <ext:JsonReader RootProperty="d">
                                                        </ext:JsonReader>
                                                    </Reader>
                                                </ext:AjaxProxy>
                                            </Proxy>
                                        </ext:Store>
                                    </Store>
                                    <Listeners>
                                        <Select Handler="#{IdCity}.clearValue();
                                                        #{CityStore}.getProxy().extraParams.IdCountry = #{IdCountry}.value;
                                                        #{CityStore}.reload();" />

                                        <ValidityChange Handler="#{IdCity}.clearValue()
                                                        #{CityStore}.getProxy().extraParams.IdCountry = #{IdCountry}.value;
                                                        #{CityStore}.reload();" />
                                    </Listeners>

                                </ext:SelectBox>
                                <ext:SelectBox ID="IdCity" runat="server" EmptyText="Город" Width="220" AllowBlank="false"
                                    DisplayField="NameCity"
                                    ValueField="IdCity">
                                    <Store>
                                        <ext:Store runat="server" ID="CityStore" AutoLoad="false">
                                            <Model>
                                                <ext:Model runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="IdCity" />
                                                        <ext:ModelField Name="NameCity" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                            <Proxy>
                                                <ext:AjaxProxy Url="/Services/PersonService.asmx/CityList" Json="true">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="IdCountry">
                                                        </ext:Parameter>
                                                    </ExtraParams>
                                                    <Reader>
                                                        <ext:JsonReader RootProperty="d">
                                                        </ext:JsonReader>
                                                    </Reader>
                                                </ext:AjaxProxy>
                                            </Proxy>
                                        </ext:Store>
                                    </Store>
                                </ext:SelectBox>
                                <ext:Toolbar runat="server">
                                    <Items>
                                        <ext:Button runat="server" Width="100" Text="Очистить" Handler="ClickPersonClear" />
                                        <ext:Button runat="server" Width="100" Text="Сохранить" Handler="ClickSave" />
                                    </Items>
                                </ext:Toolbar>
                            </Items>
                            <ErrorReader>
                                <ext:JsonReader RootProperty="d" ModelName="MessageModel" SuccessProperty="d.success">
                                </ext:JsonReader>
                            </ErrorReader>
                        </ext:FormPanel>
                        <ext:FormPanel
                            ID="SearchForm"
                            runat="server"
                            Title="Поиск сотрудник"
                            BodyPadding="20">
                            <Items>
                                <ext:TextField ID="s_LastName" runat="server" Width="220" EmptyText="Фамилия" />
                                <ext:TextField ID="s_FirstName" runat="server" Width="220" EmptyText="Имя" />
                                <ext:TextField ID="s_MiddleName" runat="server" Width="220" EmptyText="Отчество" />
                                <ext:TextField ID="s_NumberPhone" runat="server" Width="220" EmptyText="Номер мобильного телефона" />
                                <ext:TextField ID="s_Email" runat="server" Width="220" EmptyText="Адрес электронной почты" />
                                <%--                                    <Plugins>
                                        <ext:InputMask ID="InputMask2" runat="server" Mask="9 (999) 999-99-99" Placeholder="_" AlwaysShow="true" />
                                    </Plugins>--%>
                                <ext:SelectBox ID="s_IdCountry" runat="server" EmptyText="Страна" Width="220"
                                    DisplayField="NameCountry"
                                    ValueField="IdCountry">
                                    <Store>
                                        <ext:Store runat="server" ID="SearchCountryStore">
                                            <Model>
                                                <ext:Model ID="Model1" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="IdCountry" />
                                                        <ext:ModelField Name="NameCountry" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                            <Proxy>
                                                <ext:AjaxProxy Url="/Services/PersonService.asmx/CountryList" Json="true">
                                                    <Reader>
                                                        <ext:JsonReader RootProperty="d">
                                                        </ext:JsonReader>
                                                    </Reader>
                                                </ext:AjaxProxy>
                                            </Proxy>
                                        </ext:Store>
                                    </Store>
                                    <Listeners>
                                        <Select Handler="#{s_IdCity}.clearValue();
                                                        #{SearchCityStore}.getProxy().extraParams.IdCountry = #{s_IdCountry}.value;
                                                        #{SearchCityStore}.reload();" />
                                    </Listeners>

                                </ext:SelectBox>
                                <ext:SelectBox ID="s_IdCity" runat="server" EmptyText="Город" Width="220"
                                    DisplayField="NameCity"
                                    ValueField="IdCity">
                                    <Store>
                                        <ext:Store runat="server" ID="SearchCityStore" AutoLoad="false">
                                            <Model>
                                                <ext:Model ID="Model2" runat="server">
                                                    <Fields>
                                                        <ext:ModelField Name="IdCity" />
                                                        <ext:ModelField Name="NameCity" />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                            <Proxy>
                                                <ext:AjaxProxy Url="/Services/PersonService.asmx/CityList" Json="true">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="IdCountry">
                                                        </ext:Parameter>
                                                    </ExtraParams>
                                                    <Reader>
                                                        <ext:JsonReader RootProperty="d">
                                                        </ext:JsonReader>
                                                    </Reader>
                                                </ext:AjaxProxy>
                                            </Proxy>
                                        </ext:Store>
                                    </Store>
                                </ext:SelectBox>
                                <ext:Toolbar runat="server">
                                    <Items>
                                        <ext:Button runat="server" Width="100" Text="Очистить" Handler="ClickSearchClear" />
                                        <ext:Button runat="server" Width="100" Text="Поиск" Handler="ClickSearch" />
                                    </Items>
                                </ext:Toolbar>
                            </Items>
                        </ext:FormPanel>
                    </Items>
                </ext:Panel>
                <ext:Panel runat="server" MarginSpec="0 30" ColumnWidth="0.85" Title="Список сотрудников">
                    <Items>
                        <ext:GridPanel runat="server">
                            <ColumnModel runat="server">
                                <Columns>
                                    <ext:Column runat="server" Text="Фамилия" DataIndex="LastName" Width="150" />
                                    <ext:Column runat="server" Text="Имя" DataIndex="FirstName" Width="150" />
                                    <ext:Column runat="server" Text="Отчество" DataIndex="MiddleName" Width="150" />
                                    <ext:Column runat="server" Text="Номер мобильного телефона" DataIndex="NumberPhone" Width="220" />
                                    <ext:Column runat="server" Text="Адрес электронного почтового ящика" DataIndex="Email" Width="260" />
                                    <ext:Column runat="server" Text="Страна" DataIndex="NameCountry" />
                                    <ext:Column runat="server" Text="Город" DataIndex="NameCity" />

                                    <ext:CommandColumn runat="server" Text="Управление" Align="Center" Width="250">

                                        <Commands>
                                        </Commands>
                                        <Commands>
                                            <ext:GridCommand CommandName="Edit" Text="Редактировать" Icon="NoteEdit" />
                                            <ext:GridCommand CommandName="Delete" Text="Удалить" Icon="NoteDelete" />
                                        </Commands>
                                        <Listeners>
                                            <Command Handler="if (command=='Edit') 
                                                              {
                                                                #{PersonForm}.getForm().reset();
                                                                #{PersonForm}.getForm().loadRecord(record);
                                                                

                                                              };
                                                              if (command=='Delete') 
                                                              {
                                                                Ext.Msg.alert(command, record.data.LastName); 
                                                              };" />

                                        </Listeners>
                                    </ext:CommandColumn>
                                </Columns>
                            </ColumnModel>
                            <Store>
                                <ext:Store ID="PersonStore" runat="server" AutoSync="true" AutoLoad="true">
                                    <Model>
                                        <ext:Model ID="PersonModel" runat="server" IDProperty="IdPerson">
                                            <Fields>
                                                <ext:ModelField Name="IdPerson" />
                                                <ext:ModelField Name="LastName" />
                                                <ext:ModelField Name="FirstName" />
                                                <ext:ModelField Name="MiddleName" />
                                                <ext:ModelField Name="NumberPhone" />
                                                <ext:ModelField Name="Email" />
                                                <ext:ModelField Name="NameCountry" />
                                                <ext:ModelField Name="NameCity" />
                                                <ext:ModelField Name="IdCountry" />
                                                <ext:ModelField Name="IdCity" />
                                            </Fields>
                                            <Proxy>
                                                <ext:AjaxProxy Url="/Services/PersonService.asmx/PersonList" Json="true">
                                                    <Reader>
                                                        <ext:JsonReader RootProperty="d">
                                                        </ext:JsonReader>
                                                    </Reader>
                                                </ext:AjaxProxy>
                                            </Proxy>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                        </ext:GridPanel>
                    </Items>
                </ext:Panel>
            </Items>
        </ext:Panel>
    </form>
</body>
</html>
