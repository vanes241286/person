﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.Web.Services;
using NHibernate;
using PersonCompany.Models;
using Newtonsoft.Json;
using NHibernate.Criterion;

namespace PersonCompany.Services
{
    /// <summary>
    /// Сводное описание для PersonService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]

    // Чтобы разрешить вызывать веб-службу из скрипта с помощью ASP.NET AJAX, раскомментируйте следующую строку. 
    [ScriptService]
    public class PersonService : System.Web.Services.WebService
    {

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public object CountryList()
        {
            try
            {
                using (ISession session = NHibernateHelper.OpenSession())
                using (ITransaction transaction = session.BeginTransaction())
                {
                    var list = from item in session.CreateCriteria<Country>().List<Country>()
                               select new
                               {
                                   item.IdCountry,
                                   item.NameCountry
                               };

                    return list;
                }
            }
            catch (Exception e)
            {
                return new { success = false, message = e.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public object CityList()
        {
            try
            {
                string IdCountry = Context.Request.QueryString["IdCountry"];

                if (IdCountry == null || IdCountry == "")
                    throw new Exception("Не указан параметр IdCountry");

                using (ISession session = NHibernateHelper.OpenSession())
                using (ITransaction transaction = session.BeginTransaction())
                {
                    var list = from item in session.CreateCriteria<City>().Add(Expression.Eq("IdCountry", Convert.ToInt32(IdCountry))).List<City>()
                               select new
                               {
                                   item.IdCity,
                                   item.NameCity
                               };

                    return list;
                }
            }
            catch (Exception e)
            {
                return new { success = false, message = e.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public object PersonList()
        {
            try
            {
                using (ISession session = NHibernateHelper.OpenSession())
                using (ITransaction transaction = session.BeginTransaction())
                {

                    ICriteria criteria = session.CreateCriteria<Person>();

                    if (Context.Request["LastName"] != null)
                        criteria.Add(Restrictions.InsensitiveLike("LastName", Context.Request["LastName"], MatchMode.Anywhere));

                    if (Context.Request["FirstName"] != null)
                        criteria.Add(Restrictions.InsensitiveLike("FirstName", Context.Request["FirstName"], MatchMode.Anywhere));

                    if (Context.Request["MiddleName"] != null)
                        criteria.Add(Restrictions.InsensitiveLike("MiddleName", Context.Request["MiddleName"], MatchMode.Anywhere));

                    if (Context.Request["NumberPhone"] != null)
                        criteria.Add(Restrictions.InsensitiveLike("NumberPhone", Context.Request["NumberPhone"], MatchMode.Anywhere));

                    if (Context.Request["Email"] != null)
                        criteria.Add(Restrictions.InsensitiveLike("Email", Context.Request["Email"], MatchMode.Anywhere));

                    if (Context.Request["IdCountry"] != null)
                        criteria.Add(Expression.Eq("IdCountry", Convert.ToInt32(Context.Request["IdCountry"])));

                    if (Context.Request["IdCity"] != null)
                        criteria.Add(Expression.Eq("IdCity", Convert.ToInt32(Context.Request["IdCity"])));


                    var list = from item in criteria.List<Person>()
                               select new
                               {
                                   item.IdPerson,
                                   item.LastName,
                                   item.FirstName,
                                   item.MiddleName,
                                   item.NumberPhone,
                                   item.Email,
                                   item.IdCountry,
                                   item.Country.NameCountry,
                                   item.IdCity,
                                   item.City.NameCity
                               };

                    return list;
                }
            }
            catch (Exception e)
            {
                return new { success = false, message = e.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object PersonSave(string IdPerson, string LastName, string FirstName, string MiddleName, string NumberPhone, string Email, string IdCountry, string IdCity)
        {
            try
            {
                Person person = new Person()
                {
                    LastName = LastName,
                    FirstName = FirstName,
                    MiddleName = MiddleName,
                    NumberPhone = NumberPhone,
                    Email = Email,
                    IdCountry = int.Parse(IdCountry),
                    IdCity = int.Parse(IdCity)
                };
                if (IdPerson != "")
                    person.IdPerson = int.Parse(IdPerson);

                using (ISession session = NHibernateHelper.OpenSession())
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.SaveOrUpdate(person);
                    transaction.Commit();
                    return new { success = true, message = "Данные успешно сохранены" };
                }
            }
            catch (Exception e)
            {
                return new { success = false, message = e.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string PersonDel(string IdPerson)
        {
            try
            {
                if (IdPerson != "")
                {
                    using (ISession session = NHibernateHelper.OpenSession())
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Delete("from Person p where p.IdPerson=" + IdPerson);
                        transaction.Commit();
                        return "{\"success\":true}";
                    }
                }
                return "{\"success\":false}";
            }
            catch (Exception e)
            {
                return "{\"success\":false, \"\":message:" + e.Message + "}";
            }
        }
    }
}
